package com.example.Simple.File.Explorer;

import java.io.File;
import java.nio.file.Files;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route("")
@PageTitle("Simple File Explorer")
public class MainLayout extends HorizontalLayout{
	
	public MainLayout() {
		SimpleTxtEditor simpleTxtEditor = new SimpleTxtEditor();
		File userDir = new File(System.getProperty("user.home"));
		FileTree fileTree = new FileTree(userDir, simpleTxtEditor);
		simpleTxtEditor.setListener(fileTree);
		
		
		add(fileTree, simpleTxtEditor);
	}
}
