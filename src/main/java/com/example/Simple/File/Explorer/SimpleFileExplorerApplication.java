package com.example.Simple.File.Explorer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleFileExplorerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleFileExplorerApplication.class, args);
	}

}
