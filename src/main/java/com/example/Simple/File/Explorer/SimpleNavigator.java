package com.example.Simple.File.Explorer;

import java.io.File;

public class SimpleNavigator {
	
	private File root;
	private SimpleNavigator previous;
	private SimpleNavigator next;
	
	public SimpleNavigator(File root) {
		this.root = root;
	}
	
	public SimpleNavigator(SimpleNavigator  prevoius, File root) {
		this.previous = prevoius;
		this.root = root;
	}
	
	public boolean hasPrevoius() {
		if(previous != null) {
			return true;
		}
		
		return false;
	}
	
	public boolean hasNext() {
		if(next != null) {
			return true;
		}
		
		return false;
	}

	public File getRoot() {
		return root;
	}

	public void setRoot(File root) {
		this.root = root;
	}

	public SimpleNavigator getPrevious() {
		return previous;
	}

	public void setPrevious(SimpleNavigator previous) {
		this.previous = previous;
	}

	public SimpleNavigator getNext() {
		return next;
	}

	public void setNext(SimpleNavigator next) {
		this.next = next;
	}
	
}
