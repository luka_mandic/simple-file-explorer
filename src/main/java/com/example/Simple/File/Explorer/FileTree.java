package com.example.Simple.File.Explorer;

import java.io.File;
import java.io.IOException;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;



public class FileTree extends VerticalLayout {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//	Dvostrana lista za navigiranje po direktorijima
	private SimpleNavigator navigator;
	private SimpleTxtEditor editor;
	
	public FileTree(File file, SimpleTxtEditor editor) {
		this.navigator = new SimpleNavigator(file);
		this.editor = editor;
		
		update();
	}
	

	//	Gumb za prethodnu razinu
	private Button getPreviousBtn() {
		Button prev = new Button();
		prev.setIcon(new Icon(VaadinIcon.ARROW_LEFT));
		prev.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		
		if(!navigator.hasPrevoius()) {
			prev.setEnabled(false);
		}
		
		prev.addClickListener(click -> {
			navigator = navigator.getPrevious();
			
            update();
        });
		
		return prev;
	}
	
	//	Gumb za sljedecu razinu
	private Button getNextBtn() {
		Button next = new Button();
		next.setIcon(new Icon(VaadinIcon.ARROW_RIGHT));
		next.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		
		if(!navigator.hasNext()) {
			next.setEnabled(false);
		}
		
		next.addClickListener(click -> {
			navigator = navigator.getNext();
			
            update();
        });
		
		return next;
	}
	
	
	//	Gumb za novi txt file
	private Button getNewFileButton() {
		Button newFile = new Button("New file");
		
		newFile.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		
		newFile.addClickListener(click -> {
			String newFilePath = this.navigator.getRoot().getAbsolutePath() + "\\New text document.txt";
			File newTextDocument = new File(newFilePath);
			
			try {
				newTextDocument.createNewFile();
				this.update();
			} catch (IOException e) {
				Notification.show("Not created!", 1000, Notification.Position.MIDDLE);
			}
			
			try {
				this.editor.create(newTextDocument);
			} catch (Exception e) {
				Notification.show("Can't open text file!", 1000, Notification.Position.MIDDLE);
			}
			
        });
		
		return newFile;
	}
	
	//	Toolbar za gumbe prethodni i sljedeci
	private HorizontalLayout getNavigation() {
		HorizontalLayout navToolbar = new HorizontalLayout();
		navToolbar.add(getPreviousBtn(), getNextBtn());
		
		return navToolbar;
	}
	
	
	//	Traka za ispis trenutnog direktorija
	private Label getCurrentDir() {
		Label l = new Label(navigator.getRoot().getAbsolutePath());
		l.getStyle().set("border-style", "ridge");
		l.getStyle().set("padding-left", "0.5%");
		l.getStyle().set("padding-right", "0.5%");
		return l;
	}


	private void setToolbar() {
		HorizontalLayout toolbar = new HorizontalLayout();
		toolbar.getStyle().set("position", "fixed");
		toolbar.getStyle().set("z-index", "1");
		toolbar.getStyle().set("background", "white");
		
		toolbar.add(getNavigation(),getCurrentDir(), getNewFileButton());
		add(toolbar);
	}
	
	
	// Ispisuje fileove za trenutni direktorij
	public void update(){
		removeAll();
		setToolbar();
		setFileTree();
	}


	private void setFileTree() {
		if(navigator.getRoot().listFiles() == null) {
			return;
		}
		
		VerticalLayout filePanel = new VerticalLayout();
		filePanel.getStyle().set("padding-top", "4%");
		
		for (File file : navigator.getRoot().listFiles()) {
			Button b;
			
			if (file.isDirectory()) {
				b = new Button(file.getName(), new Icon(VaadinIcon.FOLDER_O));		
				b.getStyle().set("color", "Black");
			}else {
				if(file.getName().endsWith(".txt")) {
					b = new Button(file.getName(), new Icon(VaadinIcon.FILE_O));
					b.getStyle().set("color", "Black");
				}else {
					b = new Button(file.getName(), new Icon(VaadinIcon.QUESTION_CIRCLE_O));
					b.setEnabled(false);
					b.getStyle().set("color", "Gray");
				}
			}
			
			b.addThemeVariants(ButtonVariant.LUMO_SMALL);
			b.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			
			b.addClickListener(click -> {
				if(file.isDirectory()) {
					SimpleNavigator newNavigator = new SimpleNavigator(navigator, file);
					navigator.setNext(newNavigator);
					navigator = newNavigator;
				
					update();
				}
				if(file.getName().endsWith(".txt")) {
					try {
						this.editor.update(file);
					} catch (Exception e) {
						Notification.show("Can't open text file!", 1000, Notification.Position.MIDDLE);
					}
					
				}
	        });
			
			filePanel.add(b);
		}
		
		add(filePanel);
		
	}

}
