package com.example.Simple.File.Explorer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;


public class SimpleTxtEditor extends VerticalLayout{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private FileTree fileTree;
	
	private TextArea text;
	private File file;
	private TextField textName;
	
	private boolean isNewFile = false;
	
	public SimpleTxtEditor() {	
		this.getStyle().set("position", "fixed");
		this.getStyle().set("left", "45%");
	}
	
	public void setListener(FileTree fileTree) {
		this.fileTree = fileTree;
	}
	
	private void setFileNameLabel() {
		this.textName = new TextField();
		this.textName.setValue(file.getAbsolutePath());
		this.textName.setWidth("50%");
		add(this.textName);
	}
	
	private void setButtons() {
		HorizontalLayout h = new HorizontalLayout();
		h.add(getSaveButton());
		h.add(getDeleteButton());
		h.add(getCancelButton());
		
		this.add(h);
	}
	
	private void setTextArea() {
		text = new TextArea();
		text.setSizeFull();
		text.setWidth("50%");
		text.setHeight("500px");
		add(text);
	}

	private Button getSaveButton() {
		Button save = new Button("Save");
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);
		
		save.addClickListener(click -> {
			try (PrintWriter out = new PrintWriter(this.textName.getValue())) {
				
				//	Ako je promjenjeno ime novog file-a, izbrisi New Text Document.txt
				if(isNewFile) {
					isNewFile = false;
					if(! this.file.getAbsolutePath().equals(this.textName.getValue())) {
						System.out.println((this.file.delete()));
					}
				}
				
			    out.println(this.text.getValue());
			    removeAll();
			    fileTree.update();
			    Notification.show("Successfully saved", 1000, Notification.Position.MIDDLE);
			} catch (FileNotFoundException e) {
				Notification.show("Not saved", 1000, Notification.Position.MIDDLE);
			}
			
        });

		return save;
	}
	
	private Button getDeleteButton() {
		Button delete = new Button("Delete");
		delete.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
		
		delete.addClickListener(click -> {
			if(file.delete()) {
				removeAll();
				fileTree.update();
				Notification.show("Successfully deleted", 1000, Notification.Position.MIDDLE);
			}else {
				Notification.show("Not deleted!", 1000, Notification.Position.MIDDLE);
			}
		});
		
		return delete;
	}
	
	private Button getCancelButton() {
		Button cancel = new Button("Cancel");
		cancel.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		
		cancel.addClickListener(click -> {
			removeAll();
        });
		
		return cancel;
	}
	
	public void create(File file) throws IOException {
		this.isNewFile = true;
		update(file);
	}
	
	public void update(File file) throws IOException {
		this.file = file;
		
		removeAll();
		setFileNameLabel();
		setButtons();
		setTextArea();
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    String everything = sb.toString();
		    text.clear();
			text.setValue(everything);
		    
		} finally {
		    br.close();
		}
	}
}
